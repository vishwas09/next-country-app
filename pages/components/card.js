import Link from 'next/link';
import styles from './card_style.module.css';

export default function Card({name, curr, cap, flag, map, cca3}){
    return (
    <div className={styles.mCard}>
        <div className={styles.flag}>
            <img className={styles.flag_img} src={flag} alt="flag image" />
        </div>
        <div className={styles.info_area}>
            <div className={styles.info}>
                <p className={styles.name}>{name}</p>
                <p className={styles.currency}>Currency: {curr}</p>
                <p className={styles.capital}>Capital: {cap}</p>
            </div>
            <div className={styles.ref_area}>
                <a href={map} target="_blank" rel="noopener noreferrer">
                    <span className={styles.map_btn_span}>Show Map</span>
                </a>
                <Link href={"/details?country=" + cca3}>
                    <a target="_blank" rel="noopener noreferrer">
                        <span className={styles.detail_btn_span}>Detail</span>
                    </a>
                </Link>
                
            </div>
        </div>
    </div>
    );
}