import Head from 'next/head'
import styles from '../styles/home_page.module.css'
import Card from './components/card'
import reactDom from 'react-dom';
import {Player, Controls} from '@lottiefiles/react-lottie-player';


export async function getStaticProps() {

  let data_arr = [];

  const res = await fetch("https://restcountries.com/v3.1/all");
  const obj_list = await res.json();
  console.log(obj_list[0].capital);
  for(let i = 0; i < obj_list.length; i++){
    if(obj_list[i].currencies != undefined){
      var cr = Object.values(obj_list[i].currencies);
      data_arr.push({
        mName: obj_list[i].name.common,
        mCurr: cr[0].name,
        mCap: (obj_list[i].capital == undefined)?null:obj_list[i].capital,
        mFlag: obj_list[i].flags.png,
        mMap: obj_list[i].maps.googleMaps,
        mCca3: obj_list[i].cca3,
        mCca2: obj_list[i].cca2
    });
    }
    
  }

  return {
    props: {
      data_arr,
    },
  };
}

export default function Home({data_arr}) {

  function searchBtn(){
    let srch_input = document.getElementsByClassName(styles.inpBox)[0].value;
    if(srch_input !== ""){
      var result = data_arr.find(obj => {
        return (obj.mName.toUpperCase() === srch_input.toUpperCase() || obj.mCca3 === srch_input.toUpperCase() || obj.mCca2 === srch_input.toUpperCase());
      });

      if(result != undefined){
        const elem = document.getElementsByClassName(styles.cards_container)[0];
        reactDom.render((
          <Card  
            name={result.mName} curr={result.mCurr}
            cap={result.mCap} flag={result.mFlag} 
            map={result.mMap} cca3={result.mCca3}>
          </Card>
        ), elem);
      }
      else {
        const elem = document.getElementsByClassName(styles.cards_container)[0];
        reactDom.render((
          <Player
            autoplay
            loop
            src="https://assets8.lottiefiles.com/packages/lf20_4owMZE.json"
            style={{ height: '200px', width: '300px' }}
          >
            <Controls visible={false} />
        </Player>
        ), elem);
      }
    }
    else {
      alert("Please fill the search box");
    }
  }

  return (
    <div className={styles.mbody}>
      <Head>
        <title>Country App</title>
        <meta name="description" content="Simple Country details app in next js" />
      </Head>

      <div className={styles.mHeader}>
            <p>Countries</p>
      </div>

      <div className={styles.mContainer}>
            <div className={styles.search_box}>
                <input type="text" className={styles.inpBox} placeholder="Search Countries"/>
                <button type="button" className={styles.fetchBtn} onClick={searchBtn} >
                    <img className={styles.s_icon} src='https://pngimage.net/wp-content/uploads/2018/06/searchicon.png-3.png' height='25px' width='25px' alt='s_icon'/>
                </button>
            </div>

            <div className={styles.cards_container}>

              {data_arr.map(({mName, mCurr, mFlag, mCap, mMap, mCca3}) => (
              <Card key={mName} 
              name={mName} curr={mCurr} cap={mCap} flag={mFlag} map={mMap} cca3={mCca3}>
              </Card>
              ))}

            </div>   

      </div>
      
    </div>
  )
}
