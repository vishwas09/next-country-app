import styles from '../styles/details_sty.module.css';
import Head from 'next/head';
import { useRouter } from 'next/router'

const Index = () => {
    const router = useRouter();
    console.log(router.query);
  }

export async function getServerSideProps(context) {
    
    const {country} = context.query;
    const res = await fetch("https://restcountries.com/v3.1/alpha/" + country);
    const obj = await res.json();

    let native = Object.values(obj[0].name.nativeName);
    let langs = Object.values(obj[0].languages);

    let details = {
        mName: (native.length > 1) ? native[1].common : native[0].common,
        mCap: obj[0].capital,
        mPop: obj[0].population,
        mReg: obj[0].region,
        mSubReg: obj[0].subregion,
        mArea: obj[0].area + 'Km square',
        mCc: obj[0].idd.root + obj[0].idd.suffixes[0],
        mLang: (langs.length > 1) ? langs[0] + " , " + langs[1] : langs[0],
        mCurr: Object.values(obj[0].currencies)[0].name,
        mTimez: obj[0].timezones[0],
        mFlag: obj[0].flags.png
    };

    let borders = obj[0].borders;
    let neighbour_list = [];
    if(borders != null || borders != undefined){
        for(let i = 0; i<borders.length; i++){
            const _res = await fetch("https://restcountries.com/v3.1/alpha/" + borders[i]);
            const _obj = await _res.json();
            neighbour_list.push({
                id: i,
                flag:_obj[0].flags.png
            });
        }
    }

    return {
      props: {
        details,
        neighbour_list
      },
    };
  }

export default function Details({details, neighbour_list}){
    return (
        <div className={styles.mbody}>
            <Head>
                <title>Country Details</title>
            </Head>

            <div className={styles.d_container}>
                <div className={styles.detail_area}>
                    <h1 className={styles.country_name}>{details.mName}</h1>
                    <div className={styles.d_flag_img}>
                        <img className={styles.country_f_img} src={details.mFlag} alt="flag image" />
                    </div>
                    <div className={styles.country_details}>
                        <p>Native Name: {details.mName}</p>
                        <p>Capital: {details.mCap}</p>
                        <p>Population: {details.mPop}</p>
                        <p>Region: {details.mReg}</p>
                        <p>Sub-Region: {details.mSubReg}</p>
                        <p>Area: {details.mArea}</p>
                        <p>Country Code: {details.mCc}</p>
                        <p>Languages: {details.mLang}</p>
                        <p>Currencies: {details.mCurr}</p>
                        <p>Timezones: {details.mTimez}</p>
                    </div>

                </div>

                <div className={styles.neighbours}>
                    <h2>Neighbour Countries</h2>
                    
                    <div className={styles.neighbour_flags}>
                        {neighbour_list.map(({id, flag}) => (
                            <img key={id} src={flag} className={styles.nb_flags} alt="flag image" />
                        ))}
                    </div>
                </div>
                
            </div>
        </div>
    );
}